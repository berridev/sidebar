import 'package:flutter/material.dart';

TextStyle listTileDefaultTextStyle =
    TextStyle(color: Colors.white70, fontSize: 20, fontWeight: FontWeight.w600);

TextStyle listTileSelectedTextStyle =
    TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.w600);

Color selectedColor = Color(0xff4ac8ea);
Color drawerbackgroundColor = Color(0xff272d34);
