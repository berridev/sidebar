import 'package:flutter/material.dart';
import 'package:sidebar_flutter/commons/collapsing_list_tile.dart';
import 'package:sidebar_flutter/model/navigation.dart';
import 'package:sidebar_flutter/theme.dart';

class CollapsingNavigationDrawer extends StatefulWidget {
  @override
  _CollapsingNavigationDrawerState createState() =>
      _CollapsingNavigationDrawerState();
}

class _CollapsingNavigationDrawerState extends State<CollapsingNavigationDrawer>
    with SingleTickerProviderStateMixin {
  double maxWidth = 250;
  double minWidth = 70;
  bool isCollapsed = false;
  AnimationController animationControler;
  Animation<double> widthAnimation;

  @override
  void initState() {
    super.initState();
    animationControler =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    widthAnimation = Tween<double>(begin: maxWidth, end: maxWidth)
        .animate(animationControler);
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationControler,
      builder: (context, widget) => getWidget(context, widget),
    );
  }

  Widget getWidget(context, widget) {
    return Container(
      width: widthAnimation.value,
      color: drawerbackgroundColor,
      child: Column(
        children: <Widget>[
          SizedBox(height: 50),
          CollapsingListTile(
            title: "Berri Primaputa",
            icon: Icons.person,
            animationControllers: animationControler,
          ),
          Expanded(
            child: ListView.builder(
              itemBuilder: (context, counter) {
                return CollapsingListTile(
                  title: navigationItems[counter].title,
                  icon: navigationItems[counter].icon,
                  animationControllers: animationControler,
                );
              },
              itemCount: navigationItems.length,
            ),
          ),
          InkWell(
            onTap: () {
              setState(
                () {
                  isCollapsed != isCollapsed;
                  isCollapsed
                      ? animationControler.forward()
                      : animationControler.reverse();
                },
              );
            },
            child: Icon(
              Icons.chevron_left,
              color: Colors.white,
              size: 50,
            ),
          ),
        ],
      ),
    );
  }
}
