import 'package:flutter/material.dart';
import 'package:sidebar_flutter/theme.dart';

class CollapsingListTile extends StatefulWidget {
  final String title;
  final IconData icon;
  final AnimationController animationControllers;
  CollapsingListTile(
      {@required this.title,
      @required this.icon,
      @required this.animationControllers});

  @override
  _CollapsingListTileState createState() => _CollapsingListTileState();
}

class _CollapsingListTileState extends State<CollapsingListTile> {
  Animation<double> widthAnimation;
  @override
  void initState() {
    super.initState();
    widthAnimation =
        Tween<double>(begin: 250, end: 70).animate(widget.animationControllers);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widthAnimation.value,
      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: Row(
        children: <Widget>[
          Icon(widget.icon, color: Colors.white30, size: 38),
          SizedBox(width: 10),
          (widthAnimation.value >= 220)
              ? Text(widget.title, style: listTileDefaultTextStyle)
              : Container()
        ],
      ),
    );
  }
}
