import 'package:flutter/material.dart';

class NavigationModel {
  String title;
  IconData icon;
  
  NavigationModel ({this.title, this.icon});
}

List<NavigationModel> navigationItems =[
  NavigationModel(title: "Dashbord", icon: Icons.dashboard),
  NavigationModel(title: "Erors", icon: Icons.error),
  NavigationModel(title: "Search", icon: Icons.search),
  NavigationModel(title: "Notification", icon: Icons.notifications),
  NavigationModel(title: "Setting", icon: Icons.settings),
];
