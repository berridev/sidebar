import 'package:flutter/material.dart';
import 'commons/collapsing.dart';

void main() => runApp(
      MaterialApp(
        home: MyHome(),
        title: "sidebar_flutter",
        debugShowCheckedModeBanner: false,
      ),
    );

class MyHome extends StatelessWidget {
  const MyHome({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("data"),
      ),
      drawer: CollapsingNavigationDrawer(),
    );
  }
}
